﻿using BLL.Conrtact.DTO;
using BLL.Conrtact.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services
{
    public class FakeService: IPostService
    {
        public List<FormDTO> GetForm(int? filter)
        {
            List<FormDTO> FormDTOList = new List<FormDTO>();
            FormDTO newForm = new FormDTO();
            newForm.Id = 123;
            newForm.Name = "Happy";
            newForm.Email = "h1ppy@";
            FormDTOList.Add(newForm);
            return FormDTOList;
        }
    }
}
