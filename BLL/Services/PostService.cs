﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using BLL.Conrtact.DTO;
using BLL.Conrtact.Interfaces;

namespace BLL.Services
{
    
    public class PostService:IPostService
    {

        public FormRepositories formRepositories;

        public PostService(FormRepositories formRepositories)
        {
            this.formRepositories = formRepositories;
        }

        public List<FormDTO> GetForm(int? filter)
        {
            List<FormDTO> FormDTOList = new List<FormDTO>();
            formRepositories.GetForm(filter).ForEach(u =>
            {
                FormDTO newForm = new FormDTO();
                newForm.Id = u.Id;
                newForm.Name = u.Name;
                newForm.Email = u.Email;
                FormDTOList.Add(newForm);
            });
            return FormDTOList;
        }

    }

    
}
