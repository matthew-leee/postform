﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVCtrial.Models;

namespace MVCtrial.Controllers
{
    public class TrialController : Controller
    {
        private readonly MVCtrialContext _context;

        public TrialController(MVCtrialContext context)
        {
            _context = context;
        }

        // GET: Trial
        public async Task<IActionResult> Index()
        {
            return View(await _context.Trial.ToListAsync());
        }

        // GET: Trial/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trial = await _context.Trial
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trial == null)
            {
                return NotFound();
            }

            return View(trial);
        }

        // GET: Trial/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Trial/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,ReleaseDate,Genre,Price")] Trial trial)
        {
            if (ModelState.IsValid)
            {
                _context.Add(trial);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(trial);
        }

        // GET: Trial/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trial = await _context.Trial.FindAsync(id);
            
            if (trial == null)
            {
                return NotFound();
            }
            return View(trial);
        }

        // POST: Trial/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,ReleaseDate,Genre,Price")] Trial trial)
        {
            if (id != trial.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trial);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrialExists(trial.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(trial);
        }

        // GET: Trial/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trial = await _context.Trial
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trial == null)
            {
                return NotFound();
            }

            return View(trial);
        }

        // POST: Trial/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var trial = await _context.Trial.FindAsync(id);
            _context.Trial.Remove(trial);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrialExists(int id)
        {
            return _context.Trial.Any(e => e.Id == id);
        }
    }
}
