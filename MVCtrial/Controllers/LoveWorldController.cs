﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace MVCtrial.Controllers
{
    // name of the class route you like a get request
    // choose a good name for that class lol
    // configure this at startup.cs
    public class LoveWorldController : Controller
    {
        // index is the default 
        public IActionResult Index()
        {
            ViewData["Title"] = "Not Index!!!!!!";
            ViewData["Bad"] = Bad();
            return View();
        }
        public IActionResult Well (string sth)
        {
            Num += 5;
            ViewData["Title"] = "I am not well";
            ViewData["Num"] = Num;
            ViewData["Sth"] = sth;
            return View();
            // return HtmlEncoder.Default.Encode($"private int num is {num}, Who am I? {Bad()}, public int Num is {Num}, the thing that really use the encoder is here: {sth}");
        }
        public string Bad()
        {
            Num += 5;
            return "I am a bad boy, one is " + Num;
        }
        private int num = 1;
        public int Num
        {
            get
            {
                return num + 100;
            }
            set
            {
                num += 10000;
            }
        }
    }
}