﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using MVCtrial.Models;
using System.Data;
using System.Data.SqlClient;
using BLL.Services;
using BLL.Conrtact.Interfaces;

namespace MVCtrial.Controllers
{
    public class FormController : Controller
    {

        public IConfiguration Configuration { get; }

        private IPostService postService;

        public FormController (IConfiguration configuration, IPostService postService)
        {
            Configuration = configuration;
            this.postService = postService;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetForm (string filter)
        {
            HttpContext.Session.SetInt32("filter", int.Parse(filter));
            return RedirectToAction("PostForm");
        }

        [HttpGet]
        public IActionResult ClearFilter()
        {
            HttpContext.Session.Remove("filter");
            return RedirectToAction("PostForm");
        }
        
        

        [HttpGet]
        public IActionResult PostForm()
        {
            int? filter = HttpContext.Session.GetInt32("filter");
            var form = new List<Form>();

            postService.GetForm(filter).ForEach(u =>
            {
                Form newForm = new Form();
                newForm.Id = u.Id;
                newForm.Name = u.Name;
                newForm.Email = u.Email;
                form.Add(newForm);
            });
            return View(form);
        }
        

        [HttpPost]
        public IActionResult PostForm(string id, string email, string name)
        {
            int intid = int.Parse(id);
            if (ModelState.IsValid)
            {
                string connectionString = Configuration["ConnectionStrings:MVCtrialContext"];
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = $"Insert Into [Table] (Id, Name, Email) Values ('{intid}','{name}', '{email}')";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.Text;
                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                    // return RedirectToAction("PostForm");
                    return new JsonResult(new { id, name, email });
                }
            }
            else
            {
                ViewData["email"] = email;
                return new JsonResult(new { name, email });
            }
        }
    }
}