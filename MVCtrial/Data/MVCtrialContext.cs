﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MVCtrial.Models
{
    public class MVCtrialContext : DbContext
    {
        public MVCtrialContext (DbContextOptions<MVCtrialContext> options)
            : base(options)
        {
        }

        public DbSet<MVCtrial.Models.Trial> Trial { get; set; }
    }
}
