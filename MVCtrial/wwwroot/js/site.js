﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


// control the first form
document.querySelector("#PostForm").addEventListener("click", async e => {
    e.preventDefault();
    const form = document.querySelector("#FormTest1")
    const res = await fetch("/Form/PostForm", {
        method: 'POST',
        body: new FormData(form),
    })
    const json = await res.json()
    console.log("fetched email", json.email)

    const tr = document.createElement("tr")
    tr.setAttribute("accesskey", json.id)
    Object.keys(json).slice(1).forEach(u => {
        const td = document.createElement("td")
        td.innerText = json[u]
        tr.appendChild(td)
    })
    document.querySelector("tbody").appendChild(tr);
})

// control filter form
//document.querySelector("#GetForm").addEventListener("click", async e => {
//    e.preventDefault();
//    const form = document.querySelector("#FormTest2");
//    const res = await fetch("/Form/PostForm", {
//        method: "GET",
//        body: new FormData(form)
//    })
//    const json = await res.json()

//})