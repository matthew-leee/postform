﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace DAL.Repositories
{
    public class FormRepositories
    {
        public IConfiguration Configuration { get; }

        public FormRepositories(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public List<FormEntity> GetForm(int? filter)
        {
            List<FormEntity> getForm = new List<FormEntity>();
            string connectionString = Configuration["ConnectionStrings:MVCtrialContext"];
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "Select * from [Table] where LEN(Name) >= @param1";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.Add("@param1", SqlDbType.Int).Value = filter == null ? 0 : filter;
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            FormEntity form = new FormEntity
                            {
                                Id = (int)dataReader["Id"],
                                Name = Convert.ToString(dataReader["Name"]),
                                Email = Convert.ToString(dataReader["Email"])
                            };
                            getForm.Add(form);
                        }
                    }
                }

                connection.Close();
            }
            return getForm;
        }
    }
}
