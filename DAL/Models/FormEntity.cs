﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class FormEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
